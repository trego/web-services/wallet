package db

import (
	"fmt"
	"os"

	"gitlab.com/trego/web-services/wallet/model"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // PostgreSQL driver
)

var instance *gorm.DB

func initDB() (*gorm.DB, error) {
	connString := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_DATABASE"),
		os.Getenv("DB_PASSWORD"),
	)

	if os.Getenv("APP_MODE") == "dev" || os.Getenv("APP_MODE") == "test" {
		connString = fmt.Sprintf("%s sslmode=disable", connString)
	}

	return gorm.Open("postgres", connString)
}

// Get return DB connection as singleton.
func Get() *gorm.DB {
	if instance == nil {
		conn, err := initDB()
		if err != nil {
			panic(err.Error())
		}

		instance = conn
	}

	return instance
}

// Migrate migrating available model.
func Migrate() {
	Get().AutoMigrate(
		&model.MutationType{},
	)
}
