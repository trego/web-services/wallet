package db

import (
	"gitlab.com/trego/web-services/wallet/model"
)

// Seed seeding database.
func Seed() {
	models := []model.Seeder{
		&model.MutationType{},
	}

	dbConn := Get()
	for _, item := range models {
		item.Seed(dbConn)
	}
}
