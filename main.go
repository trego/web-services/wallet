package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		fmt.Printf("No .env file specified")
	}

	switch command() {
	case "serve":
		serveHTTP()
	case "migrate":
		runMigration()
		break
	case "seed":
		runSeeder()
		break
	default:
		fmt.Println("Invalid command")
	}
}

func command() string {
	args := os.Args[1:]
	if len(args) > 0 {
		return args[0]
	}

	return ""
}
