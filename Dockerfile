FROM golang:1.12-alpine as build

RUN apk add --no-cache git curl

WORKDIR /go/src/gitlab.com/trego/web-services/wallet

ADD . .

RUN go get -v github.com/golang/dep/cmd/dep
RUN dep ensure -v

RUN CGO_ENABLED=0 go build

FROM alpine

WORKDIR /usr/local/bin

COPY --from=build /go/src/gitlab.com/trego/web-services/wallet/wallet .
RUN chmod +x ./wallet

CMD ["wallet", "serve"]
