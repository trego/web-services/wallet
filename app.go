package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/trego/web-services/wallet/db"

	"github.com/go-chi/chi"
)

func createRouter() chi.Router {
	router := chi.NewRouter()

	router.Get("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := map[string]interface{}{
			"name":    "Trego/Wallet",
			"version": "1",
		}

		response, _ := json.Marshal(payload)

		w.Write(response)
	}))

	return router
}

func serveHTTP() {
	router := createRouter()
	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "8000"
	}

	fmt.Printf("App is running on port %s", port)
	http.ListenAndServe(fmt.Sprintf(":%s", port), router)
}

func runMigration() {
	fmt.Println("Running migration...")
	db.Migrate()
	fmt.Println("Migration running successfully!")
}

func runSeeder() {
	fmt.Println("Running seeder...")
	db.Seed()
	fmt.Println("Seeder running successfully!")
}
