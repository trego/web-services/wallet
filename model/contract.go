package model

import "github.com/jinzhu/gorm"

// Seeder interface
type Seeder interface {
	Seed(db *gorm.DB)
}
