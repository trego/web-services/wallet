package model

import "github.com/jinzhu/gorm"

// MutationType represent model of MutationType.
type MutationType struct {
	ID   uint
	Name string
}

// Seed is method to seed MutationType data.
func (model *MutationType) Seed(db *gorm.DB) {
	data := []MutationType{
		MutationType{
			ID:   1,
			Name: "Earnings",
		},
		MutationType{
			ID:   2,
			Name: "Withdraw",
		},
	}

	for _, item := range data {
		mutationType := MutationType{}

		db.Where("id = ?", item.ID).First(&mutationType)

		if mutationType.ID != 0 {
			db.Save(&mutationType)
			continue
		}

		mutationType.ID = 0
		db.Create(&item)
	}
}
